{extend name="base:base" /} {block name="body"} 
<div class="table-common">
    <div class="left">
        <a class="btn btn-default" href="javascript:history.back();"><i class="iconfont icon-flow"></i> 返回</a>
        <button type="submit" class="btn btn-primary ajax-post" target-form="form-horizontal"><i class="iconfont icon-tubiao_tijiao"></i> 保存</button>
    </div>
</div>
<form class="form-horizontal" action="<?php echo url('system/auth_group_edit'); ?>" method="post">
    <input type="hidden" name="id" value="{$Think.get.id}" />
    {$tpl_form}
    <div class="form-group">
        <label class="col-sm-2 control-label" for="input01">绑定菜单</label>
        <div class="col-sm-10">
            <ul id="treeDemo" class="ztree"></ul>
        </div>
    </div>
    <input type="hidden" name="menu_ids" value="<?php echo $menus; ?>" />
</form>
{/block}
{block name="foot_js"}
<link rel="stylesheet" href="__PUBLIC__/libs/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="__PUBLIC__/libs/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="__PUBLIC__/libs/zTree_v3/js/jquery.ztree.excheck.js"></script>
<SCRIPT LANGUAGE="JavaScript">
    var zTreeObj;
    // zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
    var setting = {
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onCheck: function (e, treeId, treeNode) {
                var nodes = zTreeObj.getCheckedNodes(true);
                let array = new Array();
                for (var i = 0; i < nodes.length; i++) {
                    array.push(nodes[i].id);
                }
                //为隐藏域赋值(权限id拼接字符串)
                $("input[name=menu_ids]").val(array.join(","));
            }
        }
    };
    // zTree 的数据属性，深入使用请参考 API 文档（zTreeNode 节点数据详解）
    var zNodes = [

<?php
$menus_arr = explode(',', $menus);
foreach ($menu_lists as $key => $value) {
    ?>
     {id: <?php echo $value['id']; ?>, pId: <?php echo $value['pid']; ?>, name: "<?php echo $value['name']; ?>", <?php if(in_array($value['id'],$menus_arr)){echo 'checked:true,';} ?> open:true},
<?php } ?>

    ];
    $(document).ready(function () {
        zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
    });
</SCRIPT>
{/block}