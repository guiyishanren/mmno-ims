<?php
require '../base.php';

// 目录保护
if (file_exists(__DIR__ . '/install.lock')) {
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    die(0);
}
?>
<!DOCTYPE HTML>
<html lang='zh-CN'>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>Install Guide</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="install.css" type="text/css" rel="Stylesheet" />
        
        <script type="text/javascript" charset="utf-8" src="../libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="../libs/jquery.validate/jquery.validate.js"></script>
        <script type="text/javascript" charset="utf-8" src="../libs/jquery.validate/lang-cn.js"></script>
        <script type="text/javascript" charset="utf-8" src="./install.js"></script>
    </head>
    <body>
        <div id="center">
            <div id="logo"></div>
            <div id="errorinfo"></div>
            <div id="install-main">
                <form id="sept0" class="septs">
                    <div class="clearfix" style="padding-bottom: 10px;">
                        <div id="envs-test-1" class="left" style="width: 200px;">
                            <div class="head">环境检测</div>
                            <ul>
                                <li id="version_ok">PHP7+<b></b></li>
                            </ul>
                        </div>
                        <div id="envs-test-2" class="left" style="width: 200px;">
                            <div class="head">扩展检测</div>
                            <ul>
                                <li id="ext_curl">curl</li>
                                <li id="ext_pdo_mysql">pdo_mysql</li>
                                <li id="ext_gd">gd</li>
                            </ul>
                        </div>
                        <div id="envs-test-3" class="left" style="width: 220px;">
                            <div class="head">目录读写权限</div>
                            <ul>
                                <li id="dir_uploads">/uploads/</li>
                                <li id="dir_install">/install/</li>
                                <li id="dir_extra">../application/extra/</li>
                                <li id="dir_runtime">../runtime/</li>
                            </ul>
                        </div>
                    </div>
                </form>
                <form id="sept1" class="septs">                    
                    <div class="field">
                        <div class="gs-label">数据库地址 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-dbaddress" class="required" tabindex="2" type="text" value="127.0.0.1"/><div class="gs-tip">数据库的地址：本地数据库是localhost，如果是localhost，推荐使用127.0.0.1</div></div>
                    </div>
                    <div class="field">
                        <div class="gs-label">数据库名 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-dbname" class="required" tabindex="3" type="text" value="<?php echo 'db_' . APP_THEME; ?>"/><div class="gs-tip">数据库名，不存在则自动创建，需要数据库用户具有创建数据库权限。</div></div>
                    </div>
                    <div class="field">
                        <div class="gs-label">数据库用户名 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-dbusername" class="required" tabindex="4" type="text" value="root"/><div class="gs-tip">数据库用户名：默认为root</div></div>
                    </div>
                    <div class="field">
                        <div class="gs-label">数据库密码 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-dbpassword" class="required" tabindex="5" type="password" value="" autocomplete="off"/><div class="gs-tip">数据库密码</div></div>
                    </div>
                </form>
                <form id="sept2" class="septs">
                    <div class="field">
                        <div class="gs-label">网站名称 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-title" class="required" tabindex="1" type="text" value="森洽进销存v1.0" /><div class="gs-tip">网站名称</div></div>
                    </div>
                    <div class="field">
                        <div class="gs-label">网址 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-domain" class="required" tabindex="2" type="text"/><div class="gs-tip">网址</div></div>
                    </div>                 
                    <div class="field">
                        <div class="gs-label">管理员账号 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-account" class="required" tabindex="2" type="text" value="superadmin" /><div class="gs-tip">用于系统登录</div></div>
                    </div>                 
                    <div class="field">
                        <div class="gs-label">管理员密码 <div class="gs-tip1"></div></div>
                        <div class="gs-text"><input name="f-pass" class="required" tabindex="2" type="text"/><div class="gs-tip">密码</div></div>
                    </div>                 


                </form>
                <div style="text-align: center">
                    <a class="button green" id="install-goback" href="javascript:;">上一步</a>
                    <a class="button green" id="install-btn0" href="javascript:;">环境检测</a>
                    <a class="button green" id="install-btn1" href="javascript:;">下一步</a>
                    <a class="button green" id="install-btn2" href="javascript:;">马上安装</a>                    
                </div>
            </div>
        </div>
    </body>
</html>