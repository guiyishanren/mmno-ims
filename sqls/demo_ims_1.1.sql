
-- 在菜单表中 添加一个rules字段，用于存放 权限 规则 

ALTER TABLE `tb_system_menu` ADD `rules` TEXT NULL AFTER `is_dev`;


-- 在 用户组表中 添加一个menus字段，用于 保存 用户组关联了哪些菜单

ALTER TABLE `tb_auth_group` ADD `menus` TEXT NULL AFTER `rules`;

