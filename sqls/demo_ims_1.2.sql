ALTER TABLE `tb_system_user`
CHANGE `email` `email` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `password`,
CHANGE `sex` `sex` tinyint(1) NULL AFTER `email`,
CHANGE `status` `status` tinyint(1) NOT NULL DEFAULT '0' AFTER `sex`,
CHANGE `create_time` `create_time` int(10) NULL AFTER `status`,
CHANGE `update_time` `update_time` int(10) NULL AFTER `create_time`,
CHANGE `mobile` `mobile` varchar(50) COLLATE 'utf8_general_ci' NULL AFTER `birthday`;



ALTER TABLE `tb_member`
CHANGE `birthday` `birthday` varchar(20) NULL COMMENT '生日' AFTER `id_card`;

